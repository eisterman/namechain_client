use std::error::Error;

use clap::{clap_app, crate_authors, crate_version, crate_description};
use config::Config;
use primitive_types::U256;
use log::{info};

use client_lib::config::ClientConfigBuilder;
use client_lib::start_program;

fn main() -> Result<(),Box<dyn Error>> {
    simple_logger::init().unwrap();
    let matches = clap_app!(namechain_server =>
        (version: crate_version!())
        (author: crate_authors!())
        (about: crate_description!())
        (@arg CONFIG_FILE: -c --config +takes_value "Set the TOML config file (default: config.toml)")
        (@arg SERVER: -s +takes_value
        "Sets the the NameChain server from default localhost")
        (@arg PORT: -p +takes_value
        "Sets the TCP port used for connecting with the server from default 6300")
        (@arg THREADS: -t +takes_value
        "Sets the number of threads used for the blockchain mining")
    ).get_matches();
    let config_filename =
        matches.value_of("CONFIG_FILE")
            .map_or("config.toml", |arg| arg);
    if config_filename.is_empty() {
        panic!("The config file name cannot be present with an empty name.");
    }
    let mut builder = ClientConfigBuilder::new_with_default()?;
    builder.merge_file(config_filename)?;
    let mut cli_config = Config::new();
    let difficulty: U256 = unsafe {
        let diff_string: String = builder.explicit_get("difficulty")?;
        U256::from_dec_str(diff_string.as_str()).unwrap()
    };
    info!("Difficulty Level: {}", difficulty);
    if let Some(arg) = matches.value_of("SERVER") {
        cli_config.set("server_ip", arg)?;
    }
    if let Some(arg) = matches.value_of("PORT") {
        cli_config.set("server_port", arg.parse::<i64>()?)?;
    }
    if let Some(arg) = matches.value_of("THREADS") {
        cli_config.set("number_of_mining_threads", arg.parse::<i64>()?)?;
    }
    unsafe { builder.merge_settings(cli_config)?; }
    let config = builder.build_config()?;
    info!("Number of Mining Threads: {}", config.get_number_of_mining_threads());
    start_program(config)
}
