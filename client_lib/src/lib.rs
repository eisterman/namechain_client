pub mod config;

use std::error::Error;
use std::net::TcpStream;
use std::ops::Deref;
use std::sync::{Arc, Mutex, RwLock, TryLockError};
use std::time::Duration;
use std::thread;

//use primitive_types::U256;
use log::{info, debug, error};

use namechain_utils::block::Block;
use namechain_utils::errors::NetParseError;
use namechain_utils::network::NetMessage;

use crate::config::ClientConfig;

fn get_last_block(config: &ClientConfig, stream: &mut TcpStream) -> Result<Block,Box<dyn Error>> {
    let msg = NetMessage::AskLastBlock;
    msg.send_to(stream)?;
    let reply = NetMessage::receive_from(config, stream);
    match reply {
        Ok(NetMessage::Block(block)) => {
            Ok(block)
        }
        Err(e) => {
            Err(e)
        }
        Ok(other_msg) => {
            Err(Box::new(NetParseError::new(other_msg.get_message().as_slice())))
        }
    }
}

fn send_block(config: &ClientConfig, stream: &mut TcpStream, block: &Block) -> Result<(),Box<dyn Error>> {
    let msg = NetMessage::Block(block.clone());
    msg.send_to(stream)?;
    let reply = NetMessage::receive_from(config, stream);
    match reply {
        Ok(NetMessage::ValidBlockAppended) => {
            println!("Block {} mined and sent correctly to the server!", block.to_string());
            Ok(())
        }
        Err(e) => {
            Err(e)
        }
        Ok(other_msg) => {
            //Err(Box::new(NetworkError::new(stream.peer_addr().unwrap().to_string().as_str(), block.to_string().as_str())))
            Err(Box::new(NetParseError::new(other_msg.get_message().as_slice())))
        }
    }
}

#[derive(Eq, PartialEq)]
pub enum GlobalState {
    Mining,
    OnlineBlockUpdated,
    BlockFound
}

use GlobalState::*;
use std::thread::JoinHandle;

fn spawn_last_block_thread(config: &ClientConfig, stream: &Arc<Mutex<TcpStream>>, mining_block: &Arc<RwLock<Block>>, global_state: &Arc<RwLock<GlobalState>>) -> JoinHandle<()>{
    let clone_thread_safe_stream = stream.clone();
    let clone_mining_block = mining_block.clone();
    let clone_global_state = global_state.clone();
    let clone_config = config.clone();
    thread::Builder::new().name("last_block_thread".to_string()).spawn( move || {
        loop {
            thread::sleep(Duration::from_secs(3));
            //TODO: Gestione Disconnessione forzata inattesa
            // Per farlo istanzia lo stream all'interno del thread, così se crasha, può ripristinare la situazione.
            {
                let mut locked_mining_block = clone_mining_block.write().unwrap();
                let mut locked_global_state = clone_global_state.write().unwrap();
                if matches!(*locked_global_state, Mining) {
                    let new_last_block = get_last_block(&clone_config, &mut clone_thread_safe_stream.lock().unwrap()).unwrap();
                    if locked_mining_block.ne(&new_last_block) {
                        *locked_global_state = GlobalState::OnlineBlockUpdated;
                        *locked_mining_block = new_last_block;
                    }
                }
            }
        }
    }).expect("Error spawning the last_block_thread")
}

pub fn start_mining_thread_pool(config: &ClientConfig, mining_block: &Arc<RwLock<Block>>, global_state: &Arc<RwLock<GlobalState>>) -> Vec<JoinHandle<()>> {
    let mut thread_pool = Vec::new();
    let mining_threads = config.get_number_of_mining_threads();
    let numbers_for_thread = std::u64::MAX / (mining_threads as u64);
    // Non copre tutto il range completamente, ma permette di coprire quasi tutti i nounce a meno
    //  di un centinaio per colpa della perdita di precisione nella divisione.
    for i in 0..(mining_threads as u64) {
        let clone_mining_block = mining_block.clone();
        let clone_global_state = global_state.clone();
        let clone_config = config.clone();
        let mining_thread_handler = thread::Builder::new().name(format!("mining_thread_{}", i)).spawn(move || {
            let to_mine_block = clone_mining_block.read().unwrap().clone();
            let first_nounce = (numbers_for_thread * i) as u64;
            let last_nounce = (numbers_for_thread * (i + 1)) as u64 - 1;
            let new_hash = to_mine_block.str_hash();
            let mut new_block = Block::new(&clone_config,new_hash.as_str(), clone_config.get_username().as_str(), first_nounce ).unwrap();
            while new_block.get_nounce() < last_nounce {
                {
                    let lock_result = clone_global_state.try_read();
                    match lock_result {
                        Err(TryLockError::Poisoned(_)) => {
                            panic!("Poisoned Global State!!!")
                        }
                        Ok(lock) if BlockFound == *lock || OnlineBlockUpdated == *lock => {
                            // Qualcun'altro ha trovato il blocco o è stato aggiornato
                            return;
                        }
                        _ => {}
                    }
                    //This scope is like using here: drop(lock_result);
                }
                // Questa parte di codice è fuori match per rilasciare il lock
                // Ok(Mining) | Err(WouldBlock)
                if new_block.check_difficulty_hash(&clone_config) {
                    // Blocco trovato!
                    *clone_global_state.write().unwrap() = GlobalState::BlockFound;
                    *clone_mining_block.write().unwrap() = new_block;
                    return;
                } else {
                    new_block.next_nounce();
                }
            }
            debug!("mining_thread_{} has terminated the nounce assigned without finding one valid: {} -> {}", i, first_nounce, last_nounce);
        }).unwrap_or_else(|_| panic!("Error spawning the mining_thread_{}", i));
        thread_pool.push(mining_thread_handler);
    }
    thread_pool
}

pub fn start_program(config: ClientConfig) -> Result<(),Box<dyn Error>> {
    let global_state = Arc::new(RwLock::new(GlobalState::Mining));
    let server_ip = config.get_server_ip();
    let server_port = config.get_server_port();
    let server_addr = format!("{}:{}", server_ip, server_port);
    match TcpStream::connect(server_addr) {
        Ok(mut stream) => {
            info!("Successfully connected to server {} in port {}", server_ip, server_port);

            let mining_block = get_last_block(&config, &mut stream).unwrap();
            info!("The last block is {}", mining_block);

            // Preparo il thread di aggiornamento last block
            let thread_safe_stream = Arc::new(Mutex::new(stream));
            let mining_block = Arc::new(RwLock::new(mining_block));
            let _last_block_thread_handler = spawn_last_block_thread(&config, &thread_safe_stream, &mining_block, &global_state);

            info!("Begin mining process...");
            loop {
                let start = std::time::Instant::now();
                // Spawno gli n thread per il mining
                let thread_pool = start_mining_thread_pool(&config, &mining_block, &global_state);

                thread_pool.into_iter().for_each(|h| {h.join().unwrap();} );
                match *global_state.read().unwrap() {
                    BlockFound => {
                        let mined_block_guard = mining_block.read().unwrap();
                        print!("Elapsed Time: {:?} - ", start.elapsed());
                        send_block(&config, &mut thread_safe_stream.lock().unwrap(), mined_block_guard.deref()).unwrap();
                    },
                    OnlineBlockUpdated => {
                        info!("New block on the chain from the server: {}", *mining_block.read().unwrap());
                    },
                    Mining => {
                        unreachable!()
                    }
                }
                *global_state.write().unwrap() = Mining;
            }

        },
        Err(e) => {
            error!("Failed to connect: {}", e);
            Err(Box::new(e))
        }
    }
}