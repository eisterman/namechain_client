use client_lib_proc_macro::generate_clientconfig_utils;

//TODO: Implement Verbosity in the main code
//TODO: Implement a true log system

generate_clientconfig_utils! {
    difficulty: U256 = "1766847064778384329583297500742918515827483896875618958121606201292619776";
    block_max_len: usize = 100 from 87;
    username: String = "miner";
    number_of_mining_threads: usize = 1 from 1;
    server_ip: String = "127.0.0.1";
    server_port: u16 = 6300 from 1 to 65535;
    verbosity: u8 = 2 from 0 to 5;
}