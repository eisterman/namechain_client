use criterion::{black_box, criterion_group, criterion_main, Criterion, BenchmarkId, BatchSize};
use std::sync::{Arc,RwLock};

use client_lib::GlobalState;
use client_lib::start_mining_thread_pool;

use namechain_utils::block::Block;

mod bench_utils;
use bench_utils::{get_default_config, DEFAULT_DIFFICULTY};
use std::time::{Duration, Instant};

pub fn blocks_processing_time(c: &mut Criterion) {
    const NBLOCKS: usize = 40;
    let mut group = c.benchmark_group(format!("{}_blocks_processing_time", NBLOCKS));
    group.sample_size(20);
    for i in 1..=8 {
        group.bench_with_input(BenchmarkId::from_parameter(i), &i, |b, &i| {
            b.iter_custom(|iters| {
                let mut total_time = Duration::new(0, 0);
                for _i in 0..iters {
                    let config = get_default_config(DEFAULT_DIFFICULTY, i);
                    let global_state = Arc::new(RwLock::new(GlobalState::Mining));
                    let mining_block = Arc::new(RwLock::new(
                        Block::from_str(&config,"0000000000000000000000000000000000000000000000000000000000000000 coinbase 8120").unwrap()));
                    // True bench
                    let start = Instant::now();
                    for _j in 0..NBLOCKS {
                        let thread_pool = start_mining_thread_pool(&config, &mining_block, &global_state);
                        thread_pool.into_iter().for_each(|h| { h.join().unwrap(); });
                        assert!(matches!(*global_state.read().unwrap(), GlobalState::BlockFound));
                        *global_state.write().unwrap() = GlobalState::Mining;
                    }
                    total_time += start.elapsed();
                }
                total_time
            });
        });
    }
}

criterion_group!(block_elaboration_speed, blocks_processing_time);

criterion_main!(block_elaboration_speed);

//TODO: Il benchmark del multithread è molto difficile da realizzare tenendo in conto l'esistenza
// di una componente network nella catena di controllo finale del risultato, ma la cosa positiva
// riguardo questo dettaglio è che la velocità di risposta NON DIPENDE DAL MULTITHREAD DEL CLIENT
// MA ESCLUSIVAMENTE DAL NETCODE DI SERVER E CLIENT, quindi testabile a parte (e già con bench
// all'interno di namechain_utils).
// Così ho deciso di optare per la stima di un INDICATORE DI PRESTAZIONI, ovvero alla Hash Speed,
// ovvero la quantità di hash al secondo che possono venire calcolati. Normalmente uno si
// aspetterebbe con un sistema 8 thread su 8 core logici un hash speed di 8 volte quella ottenuta
// a thread singolo, ma sappiamo che non sarà mai così ma più probabilmente il numero si attesterà
// attorno al 5-6x volte più veloce del single-thread (differenza core fisici e logici e presenza
// di un intensa quantità di operazioni matematiche).
// Il benchmark dovrà calcolare l'hash di una quantità di blocchi prefissata (DA SCEGLIERE A MANO
// E CON CURA IN BASE ALLA DIFFICOLTA SCELTA) comunque non sufficiente a causare l'esaurimento di
// uno dei thread di elaborazione (ad esempio, mentre Thread 1,2,4 lavorano, il 3 ha finito i numeri
// da testare e si ferma aspettando gli altri tre thread).

