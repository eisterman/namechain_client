use namechain_utils::config::CommonConfig;
use primitive_types::U256;
use client_lib::config::{ClientConfig, ClientConfigBuilder};

#[derive(Clone)]
pub struct TestConfig {
    difficulty: U256,
    block_max_len: usize
}

impl CommonConfig for TestConfig {
    #[inline]
    fn get_difficulty(&self) -> U256 {
        self.difficulty
    }

    #[inline]
    fn get_block_max_len(&self) -> usize {
        self.block_max_len
    }
}

impl TestConfig {
    pub fn new(difficulty: U256, block_max_len: usize) -> Self {
        assert!(block_max_len >= 87); //SHA3-256 one_letter_username u64_20cifre_nounce
        TestConfig{ difficulty, block_max_len }
    }
}

impl Default for TestConfig {
    fn default() -> Self {
        let difficulty: U256 = U256::pow(2.into(), (256-17).into()); // Più è basso, più è difficile
        Self::new(difficulty, 100)
    }
}

pub fn get_default_config(difficulty: &str, mining_threads: u64) -> ClientConfig {
    let mut builder = ClientConfigBuilder::new_with_default().unwrap();
    unsafe {
        builder.explicit_set("difficulty", difficulty).unwrap();
        builder.explicit_set("username", "benchmark").unwrap();
        builder.explicit_set("number_of_mining_threads", mining_threads as i64).unwrap();
        builder.explicit_set("server_ip", "127.0.0.1").unwrap();
        builder.explicit_set("verbosity", 0).unwrap();
    }
    builder.build_config().unwrap()
}

pub const DEFAULT_DIFFICULTY: &str = "1766847064778384329583297500742918515827483896875618958121606201292619776";